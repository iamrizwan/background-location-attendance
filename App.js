/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ToastAndroid,
  Alert,
  Switch,
  Geolocation,
  PermissionsAndroid,
} from 'react-native';

import BackgroundTask from 'react-native-background-task'
import BackgroundTimer from 'react-native-background-timer';

BackgroundTask.define(async () => {
  const response = await fetch('http://feeds.bbci.co.uk/news/rss.xml')
  const text = await response.text();
  BackgroundTask.finish()
})


type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.intervalID = null ;
  }


  state ={
    toggle:false,
    latitude:'',
    longitude:'',
    error:''
  }

//   async  requestCameraPermission() {
//   try {
//     const granted = await PermissionsAndroid.request(
//       PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
//       {
//         'title': 'location',
//         'message': 'on your location service'
//       }
//     )
//     console.log(granted);
//     console.log(PermissionsAndroid.RESULTS.GRANTED);
//       if (granted) {
//      ToastAndroid.show('granted', ToastAndroid.SHORT);
//     } else {
//       ToastAndroid.show('denied', ToastAndroid.SHORT);
//     }
//   } catch (err) {
//     console.warn(err)
//   }
// }

  getLocation(){
    navigator.geolocation.getCurrentPosition(
          (position) => {
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              error: '',
            },()=>{
              // ToastAndroid.show(this.state.latitude+ ' ' + this.state.longitude, ToastAndroid.SHORT);
              console.log(this.state.latitude+ ' ' + this.state.longitude);
            });
          },
          (error) => this.setState({error:error.message}),
          { enableHighAccuracy: true, timeout: 20000},
        );
  }

  componentDidMount() {
      this.getLocation();
  }

 async checkStatus() {
     const status = await BackgroundTask.statusAsync()
      if (status.available) {
      let counter  = 0 ;
        this.intervalID = BackgroundTimer.setInterval(() => {
          if (counter < 8) {
              BackgroundTimer.start()
            this.getLocation();
            counter++
          }else {
            BackgroundTimer.clearInterval(this.intervalID);
            //let's see if git is working
          }

        }, 3600000);

       }
   if (reason === BackgroundTask.UNAVAILABLE_DENIED) {
     Alert.alert('Denied', 'Please enable background "Background App Refresh" for this app')
   } else if (reason === BackgroundTask.UNAVAILABLE_RESTRICTED) {
     Alert.alert('Restricted', 'Background tasks are restricted on your device')
   }
}
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
            Make Your Attendence bro
        </Text>
        <Switch value={this.state.toggle} disabled={false} onValueChange={this.hanldeAttendence.bind(this)}/>
        <Text style={styles.instructions}>latitude : {this.state.latitude}</Text>
        <Text style={styles.instructions}>longitude : {this.state.longitude}</Text>
        <Text style={styles.instructions}>{this.state.error === '' ? '' : this.state.error }</Text>
      </View>
    );
  }

  hanldeAttendence = ()=> {
    this.setState({toggle:!this.state.toggle},()=>{
      if (this.state.toggle) {
        this.checkStatus()
      }
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
